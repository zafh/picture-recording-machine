package agents;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import general.XML;
import jadex.bridge.IInternalAccess;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;

@Agent
public class MqttAgentBDI {
		
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
    
	protected String MqttBroker = "141.59.143.42:1883";
	protected String MqttUser = "";
	protected String MqttPw = "";
	protected MqttClient client;
	
	//-------- beliefs --------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
        
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {
		
		// Start MQTT-Client
		try { 
			client = new MqttClient("tcp://" + MqttBroker, agent.getComponentIdentifier().toString());				 
			MqttConnectOptions options = new MqttConnectOptions();
							   options.setAutomaticReconnect(true);
							   options.setCleanSession(true);
							   options.setConnectionTimeout(10);	
							   options.setUserName("user");
							   options.setPassword("password".toCharArray());
			client.connect(options);
			client.subscribe("HardwareAgent");
			System.out.println("");
			System.out.println("Connection to MQTT-Broker (" + MqttBroker + ") established.");
		} catch (MqttException e) { 
			System.out.println("");
			System.out.println("Connection to MQTT-Broker (" + MqttBroker + ") failed!");
			System.out.println(e.getMessage());	
		}	
		
		System.out.println("MQTT-Agent has been initialized.");
		System.out.println("");
		
		// Initialisierung MQTT-Listener
		client.setCallback(new MqttCallback() {
			
	        @Override
	        public void connectionLost(Throwable throwable) { }
	  
	        @Override
	        public void messageArrived(String t, MqttMessage m) throws Exception {        		        	
	        }

			@Override
			public void deliveryComplete(IMqttDeliveryToken arg0) { }
	        
	     });
		
	}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() throws MqttException {

		client.disconnect();
		
	}
		
	//-------- goals --------
	
	//-------- plans --------
	
	//-------- methods --------
	
	public void SendMessage(boolean result) {
		
		String topic = "PictureRecordingMachineAgent";
		
		String message = XML.createMessage(
				"Inform(Hardware)", 
				"HardwareAgent", 
				"PictureRecordingMachineAgent", 
				String.valueOf(result)
				);

		try {
			client.publish(topic, new MqttMessage(message.getBytes()));
		} catch (MqttException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public static void BusyWait(long micros) {
		
		// Quelle: "busyWaitMicros"
		// http://www.rationaljava.com/2015/10/measuring-microsecond-in-java.html, 03.09.2019
		// Datum: 28.10.2015
		
        long waitUntil = System.nanoTime() + (micros * 1_000);
        
        while(waitUntil > System.nanoTime()) { ; }
        
    }

	//-------- services --------
	
}