package agents;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class PictureRecordingMachineGUI {

	//-------- window objects --------
	
	public JFrame frmPictureRecordingMachine;
	
	//-------- constructor --------
	
	public PictureRecordingMachineGUI() {
		initialize();
	}
	
	//-------- attributes --------
	
	public JTextArea taStatus; 
	public JTextField tfNumberOfImages;
	public JTextField tfAnglesOfRations;
	public JButton btnConfiguration_SavePathPictures;
	public JButton btnRecording_Stop;
	public JButton btnRecording_Start;
	public JButton btnData_Apply;
	public JButton btnData_New;
	public JButton btnData_Edit;
	public DefaultListModel<String> listModel_ArticleList = new DefaultListModel<String>();
	public JList<String> listData_ArticleList;
	public JTextField tfRecording_Turntable_Intermediate;
	public JTextField tfRecording_Rocker_Intermediate;
	public JTextField tfRecording_Turntable_Positions;
	public JTextField tfRecording_Rocker_Positions;
	public JTextField tfRecording_Rocker_RangeMax;
	public JTextField tfRecording_Turntable_RangeMax;
	public JTextField tfRecording_Rocker_RangeMin;		
	public JTextField tfRecording_Turntable_RangeMin;
	public JLabel lblRecording_Article;
	public JTextField tfData_ID;
	public JLabel lblConfiguration_SavePathPictures;
	public JCheckBox cbRecording_HardwareTest;
	public JTextField tfData_Name;
	public JTextField tfData_Location;
	public JTextField tfData_Pictures;
	public JTextField tfData_Mass;
	public JTextField tfData_Width;
	public JTextField tfData_Height;
	public JTextField tfData_Length;
	public JTextField tfData_Group;
	public JTextField tfConfiguration_SavePathPictures;
	public JComboBox<Boolean> cbControl_RockerDirection;
	public JComboBox<Boolean> cbControl_TurntableDirection;
	public JTextField txtControl_TurntableAngle;
	public JTextField txtControl_RockerAngle;
	public JButton btnControl_MoveTurntable;
	public JButton btnControl_MoveRocker;
	public JCheckBox cbRecording_CompleteTurning;
		
	private JLabel lblRecording_Rocker_AngleRange;
	private JLabel lblRecording_Rocker_Intermediate;
	private JLabel lblRecording_Turntable_Positions;
	private JLabel lblRecording_Rocker_Positions;
	private JLabel lblData_Location;
	private JLabel lblData_Pictures;
	private JLabel lblData_Mass;
	private JLabel lblData_Width;
	private JLabel lblData_Height;
	private JLabel lblData_Length;
	private JLabel lblData_Group;
	private JLabel lblData_ID;
	private JLabel lblData_Name;
	private JLabel lblData_Picture;
	private JLabel lblRecording_Turntable_AngleRange;
	private JLabel lblRecording_Turntable_Intermediate;
	private JLabel lblRecording_Turntable;	
	private JLabel lblRecording_Rocker;
	private JPanel TabConfiguration;
	private JPanel TabControl;

	//-------- methods --------
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		
		frmPictureRecordingMachine = new JFrame();
		frmPictureRecordingMachine.setResizable(false);
		frmPictureRecordingMachine.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmPictureRecordingMachine.setTitle("PRM (Picture Recording Machine)");
		frmPictureRecordingMachine.setBounds(100, 100, 886, 575);
		
		JPanel StatusDisplay = new JPanel();
		
		JTabbedPane TabMenu = new JTabbedPane(JTabbedPane.TOP);
		TabMenu.setName("");
		frmPictureRecordingMachine.getContentPane().setLayout(new BorderLayout(0, 0));
		StatusDisplay.setLayout(new BorderLayout(0, 0));
		
		JScrollPane spStatus = new JScrollPane();
		StatusDisplay.add(spStatus, BorderLayout.CENTER);
		
		taStatus = new JTextArea();
		taStatus.setEditable(false);
		taStatus.setRows(3);
		taStatus.setForeground(Color.BLACK);
		spStatus.setViewportView(taStatus);
		frmPictureRecordingMachine.getContentPane().add(StatusDisplay, BorderLayout.SOUTH);
		frmPictureRecordingMachine.getContentPane().add(TabMenu, BorderLayout.CENTER);
		
		JPanel TabData = new JPanel();
		TabMenu.addTab("Data", null, TabData, null);
		
		tfNumberOfImages = new JTextField();
		tfNumberOfImages.setBounds(1198, 456, 63, 22);
		tfNumberOfImages.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfNumberOfImages.setColumns(10);
		
		tfAnglesOfRations = new JTextField();
		tfAnglesOfRations.setBounds(1198, 484, 63, 22);
		tfAnglesOfRations.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfAnglesOfRations.setColumns(10);
		
		JPanel panelData_ArticleList = new JPanel();
		panelData_ArticleList.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelData_ArticleList.setBounds(10, 11, 209, 402);
		
		btnData_Edit = new JButton("Edit");
		btnData_Edit.setEnabled(false);
		btnData_Edit.setBounds(83, 424, 63, 23);
		
		btnData_New = new JButton("New");
		btnData_New.setEnabled(false);
		btnData_New.setBounds(10, 424, 63, 23);
		
		tfData_Location = new JTextField();
		tfData_Location.setBorder(null);
		tfData_Location.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Location.setBounds(328, 339, 195, 22);
		tfData_Location.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Location.setEditable(false);
		tfData_Location.setColumns(10);
		
		lblData_Location = new JLabel("Location:");
		lblData_Location.setBounds(239, 337, 82, 27);
		lblData_Location.setFont(new Font("Calibri", Font.BOLD, 12));
		
		lblData_Pictures = new JLabel("Pictures:");
		lblData_Pictures.setBounds(239, 296, 82, 27);
		lblData_Pictures.setFont(new Font("Calibri", Font.BOLD, 12));
		
		tfData_Pictures = new JTextField();
		tfData_Pictures.setBorder(null);
		tfData_Pictures.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Pictures.setBounds(328, 298, 124, 22);
		tfData_Pictures.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Pictures.setEditable(false);
		tfData_Pictures.setColumns(10);
		
		tfData_Mass = new JTextField();
		tfData_Mass.setBorder(null);
		tfData_Mass.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Mass.setBounds(328, 257, 124, 22);
		tfData_Mass.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Mass.setEditable(false);
		tfData_Mass.setColumns(10);
		
		lblData_Mass = new JLabel("Mass [g]:");
		lblData_Mass.setBounds(239, 255, 82, 27);
		lblData_Mass.setFont(new Font("Calibri", Font.BOLD, 12));
		
		lblData_Width = new JLabel("Width [mm]:");
		lblData_Width.setBounds(239, 214, 82, 27);
		lblData_Width.setFont(new Font("Calibri", Font.BOLD, 12));
		
		tfData_Width = new JTextField();
		tfData_Width.setBorder(null);
		tfData_Width.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Width.setBounds(328, 216, 124, 22);
		tfData_Width.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Width.setEditable(false);
		tfData_Width.setColumns(10);
		
		tfData_Height = new JTextField();
		tfData_Height.setBorder(null);
		tfData_Height.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Height.setBounds(328, 175, 124, 22);
		tfData_Height.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Height.setEditable(false);
		tfData_Height.setColumns(10);
		
		lblData_Height = new JLabel("Height [mm]:");
		lblData_Height.setBounds(239, 173, 82, 27);
		lblData_Height.setFont(new Font("Calibri", Font.BOLD, 12));
		
		lblData_Length = new JLabel("Length [mm]:");
		lblData_Length.setBounds(239, 132, 82, 27);
		lblData_Length.setFont(new Font("Calibri", Font.BOLD, 12));
		
		tfData_Length = new JTextField();
		tfData_Length.setBorder(null);
		tfData_Length.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Length.setBounds(328, 134, 124, 22);
		tfData_Length.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Length.setEditable(false);
		tfData_Length.setColumns(10);
		
		tfData_Group = new JTextField();
		tfData_Group.setBorder(null);
		tfData_Group.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Group.setBounds(328, 93, 124, 22);
		tfData_Group.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Group.setEditable(false);
		tfData_Group.setColumns(10);
		
		lblData_Group = new JLabel("Group:");
		lblData_Group.setBounds(239, 91, 82, 27);
		lblData_Group.setFont(new Font("Calibri", Font.BOLD, 12));
		
		lblData_ID = new JLabel("ID");
		lblData_ID.setBounds(239, 50, 82, 27);
		lblData_ID.setFont(new Font("Calibri", Font.BOLD, 12));
		
		tfData_ID = new JTextField();
		tfData_ID.setBorder(null);
		tfData_ID.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_ID.setBounds(328, 52, 124, 22);
		tfData_ID.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_ID.setEditable(false);
		tfData_ID.setColumns(10);
		
		tfData_Name = new JTextField();
		tfData_Name.setBorder(null);
		tfData_Name.setHorizontalAlignment(SwingConstants.LEFT);
		tfData_Name.setBounds(328, 12, 195, 22);
		tfData_Name.setFont(new Font("Calibri", Font.PLAIN, 12));
		tfData_Name.setEditable(false);
		tfData_Name.setColumns(10);
		
		lblData_Name = new JLabel("Name");
		lblData_Name.setBounds(239, 11, 82, 25);
		lblData_Name.setFont(new Font("Calibri", Font.BOLD, 12));
		panelData_ArticleList.setLayout(new BorderLayout(0, 0));
		TabData.setLayout(null);
		TabData.add(btnData_Edit);
		TabData.add(panelData_ArticleList);
		
		listData_ArticleList = new JList<String>(listModel_ArticleList);
		listData_ArticleList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panelData_ArticleList.add(listData_ArticleList, BorderLayout.CENTER);
		TabData.add(lblData_Name);
		TabData.add(tfData_Name);
		TabData.add(lblData_ID);
		TabData.add(tfData_ID);
		TabData.add(lblData_Group);
		TabData.add(tfData_Group);
		TabData.add(lblData_Length);
		TabData.add(tfData_Length);
		TabData.add(lblData_Height);
		TabData.add(tfData_Height);
		TabData.add(lblData_Width);
		TabData.add(tfData_Width);
		TabData.add(lblData_Mass);
		TabData.add(tfData_Mass);
		TabData.add(lblData_Pictures);
		TabData.add(tfData_Pictures);
		TabData.add(lblData_Location);
		TabData.add(tfData_Location);
		TabData.add(tfNumberOfImages);
		TabData.add(tfAnglesOfRations);
		TabData.add(btnData_New);
		
		btnData_Apply = new JButton("Apply");
		btnData_Apply.setEnabled(false);
		btnData_Apply.setBounds(156, 424, 63, 23);
		TabData.add(btnData_Apply);
		
		lblData_Picture = new JLabel("Picture");
		lblData_Picture.setEnabled(false);
		lblData_Picture.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblData_Picture.setHorizontalAlignment(SwingConstants.CENTER);
		lblData_Picture.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblData_Picture.setBounds(533, 11, 332, 356);
		TabData.add(lblData_Picture);
		
		JPanel TabRecording = new JPanel();
		TabMenu.addTab("Recording", null, TabRecording, null);
		TabRecording.setLayout(null);
		
		tfRecording_Rocker_RangeMin = new JTextField();
		tfRecording_Rocker_RangeMin.setText("0");
		tfRecording_Rocker_RangeMin.setHorizontalAlignment(SwingConstants.CENTER);
		tfRecording_Rocker_RangeMin.setEditable(false);
		tfRecording_Rocker_RangeMin.setColumns(10);
		tfRecording_Rocker_RangeMin.setBounds(105, 197, 42, 20);
		TabRecording.add(tfRecording_Rocker_RangeMin);
		
		tfRecording_Rocker_Intermediate = new JTextField();
		tfRecording_Rocker_Intermediate.setHorizontalAlignment(SwingConstants.CENTER);
		tfRecording_Rocker_Intermediate.setColumns(10);
		tfRecording_Rocker_Intermediate.setBounds(105, 228, 42, 20);
		TabRecording.add(tfRecording_Rocker_Intermediate);
		
		tfRecording_Rocker_RangeMax = new JTextField();
		tfRecording_Rocker_RangeMax.setText("90");
		tfRecording_Rocker_RangeMax.setHorizontalAlignment(SwingConstants.CENTER);
		tfRecording_Rocker_RangeMax.setColumns(10);
		tfRecording_Rocker_RangeMax.setBounds(157, 197, 42, 20);
		TabRecording.add(tfRecording_Rocker_RangeMax);
		
		tfRecording_Turntable_Intermediate = new JTextField();
		tfRecording_Turntable_Intermediate.setHorizontalAlignment(SwingConstants.CENTER);
		tfRecording_Turntable_Intermediate.setColumns(10);
		tfRecording_Turntable_Intermediate.setBounds(105, 104, 42, 20);
		TabRecording.add(tfRecording_Turntable_Intermediate);
		
		tfRecording_Turntable_RangeMin = new JTextField();
		tfRecording_Turntable_RangeMin.setText("0");
		tfRecording_Turntable_RangeMin.setHorizontalAlignment(SwingConstants.CENTER);
		tfRecording_Turntable_RangeMin.setEditable(false);
		tfRecording_Turntable_RangeMin.setColumns(10);
		tfRecording_Turntable_RangeMin.setBounds(105, 73, 42, 20);
		TabRecording.add(tfRecording_Turntable_RangeMin);
		
		tfRecording_Turntable_RangeMax = new JTextField();
		tfRecording_Turntable_RangeMax.setText("360");
		tfRecording_Turntable_RangeMax.setHorizontalAlignment(SwingConstants.CENTER);
		tfRecording_Turntable_RangeMax.setColumns(10);
		tfRecording_Turntable_RangeMax.setBounds(157, 73, 42, 20);
		TabRecording.add(tfRecording_Turntable_RangeMax);
		
		btnRecording_Start = new JButton("Start");
		btnRecording_Start.setBounds(20, 320, 93, 23);
		TabRecording.add(btnRecording_Start);

		lblRecording_Turntable_AngleRange = new JLabel("Angle-Range");
		lblRecording_Turntable_AngleRange.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Turntable_AngleRange.setBounds(20, 74, 83, 20);
		TabRecording.add(lblRecording_Turntable_AngleRange);
		
		lblRecording_Turntable_Intermediate = new JLabel("Intermediate");
		lblRecording_Turntable_Intermediate.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Turntable_Intermediate.setBounds(20, 105, 83, 20);
		TabRecording.add(lblRecording_Turntable_Intermediate);
		
		lblRecording_Turntable = new JLabel("Turntable");
		lblRecording_Turntable.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Turntable.setBounds(10, 42, 67, 20);
		TabRecording.add(lblRecording_Turntable);
		
		lblRecording_Article = new JLabel("Article (ID)");
		lblRecording_Article.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Article.setBounds(10, 11, 266, 20);
		TabRecording.add(lblRecording_Article);
		
		lblRecording_Rocker = new JLabel("Rocker");
		lblRecording_Rocker.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Rocker.setBounds(10, 166, 67, 20);
		TabRecording.add(lblRecording_Rocker);
		
		lblRecording_Rocker_AngleRange = new JLabel("Angle-Range");
		lblRecording_Rocker_AngleRange.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Rocker_AngleRange.setBounds(20, 198, 83, 20);
		TabRecording.add(lblRecording_Rocker_AngleRange);
		
		lblRecording_Rocker_Intermediate = new JLabel("Intermediate");
		lblRecording_Rocker_Intermediate.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Rocker_Intermediate.setBounds(20, 228, 83, 20);
		TabRecording.add(lblRecording_Rocker_Intermediate);
		
		btnRecording_Stop = new JButton("Stop");
		btnRecording_Stop.setEnabled(false);
		btnRecording_Stop.setBounds(20, 354, 93, 23);
		TabRecording.add(btnRecording_Stop);
		
		tfRecording_Turntable_Positions = new JTextField();
		tfRecording_Turntable_Positions.setBorder(null);
		tfRecording_Turntable_Positions.setHorizontalAlignment(SwingConstants.LEFT);
		tfRecording_Turntable_Positions.setEditable(false);
		tfRecording_Turntable_Positions.setColumns(10);
		tfRecording_Turntable_Positions.setBounds(105, 135, 692, 20);
		TabRecording.add(tfRecording_Turntable_Positions);
		
		tfRecording_Rocker_Positions = new JTextField();
		tfRecording_Rocker_Positions.setBorder(null);
		tfRecording_Rocker_Positions.setHorizontalAlignment(SwingConstants.LEFT);
		tfRecording_Rocker_Positions.setEditable(false);
		tfRecording_Rocker_Positions.setColumns(10);
		tfRecording_Rocker_Positions.setBounds(105, 259, 692, 20);
		TabRecording.add(tfRecording_Rocker_Positions);

		lblRecording_Turntable_Positions = new JLabel("Positions");
		lblRecording_Turntable_Positions.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Turntable_Positions.setBounds(20, 135, 78, 20);
		TabRecording.add(lblRecording_Turntable_Positions);
		
		lblRecording_Rocker_Positions = new JLabel("Positions");
		lblRecording_Rocker_Positions.setFont(new Font("Calibri", Font.BOLD, 12));
		lblRecording_Rocker_Positions.setBounds(20, 259, 83, 20);
		TabRecording.add(lblRecording_Rocker_Positions);
		
		cbRecording_HardwareTest = new JCheckBox("without camera");
		cbRecording_HardwareTest.setFont(new Font("Calibri", Font.PLAIN, 12));
		cbRecording_HardwareTest.setBounds(119, 321, 111, 23);
		TabRecording.add(cbRecording_HardwareTest);
		
		cbRecording_CompleteTurning = new JCheckBox("complete turning");
		cbRecording_CompleteTurning.setFont(new Font("Calibri", Font.PLAIN, 12));
		cbRecording_CompleteTurning.setBounds(214, 73, 123, 23);
		TabRecording.add(cbRecording_CompleteTurning);
		
		TabConfiguration = new JPanel();
		TabMenu.addTab("Configuration", null, TabConfiguration, null);
		TabConfiguration.setLayout(null);
		
		lblConfiguration_SavePathPictures = new JLabel("Save-Path (Pictures)");
		lblConfiguration_SavePathPictures.setFont(new Font("Calibri", Font.BOLD, 12));
		lblConfiguration_SavePathPictures.setBounds(10, 11, 124, 20);
		TabConfiguration.add(lblConfiguration_SavePathPictures);
		
		tfConfiguration_SavePathPictures = new JTextField();
		tfConfiguration_SavePathPictures.setBorder(null);
		tfConfiguration_SavePathPictures.setText("C:\\\\Photoneo\\\\PRM-Data\\\\");
		tfConfiguration_SavePathPictures.setEditable(false);
		tfConfiguration_SavePathPictures.setHorizontalAlignment(SwingConstants.LEFT);
		tfConfiguration_SavePathPictures.setColumns(10);
		tfConfiguration_SavePathPictures.setBounds(172, 11, 527, 20);
		TabConfiguration.add(tfConfiguration_SavePathPictures);
		
		btnConfiguration_SavePathPictures = new JButton("...");
		btnConfiguration_SavePathPictures.setEnabled(false);
		btnConfiguration_SavePathPictures.setBounds(130, 10, 32, 21);
		TabConfiguration.add(btnConfiguration_SavePathPictures);
		
		TabControl = new JPanel();
		TabMenu.addTab("Control", null, TabControl, null);
		TabControl.setLayout(null);
		
		txtControl_TurntableAngle = new JTextField();
		txtControl_TurntableAngle.setBounds(83, 34, 60, 20);
		txtControl_TurntableAngle.setHorizontalAlignment(SwingConstants.CENTER);
		txtControl_TurntableAngle.setColumns(10);
		TabControl.add(txtControl_TurntableAngle);
		
		txtControl_RockerAngle = new JTextField();
		txtControl_RockerAngle.setBounds(83, 178, 60, 20);
		txtControl_RockerAngle.setHorizontalAlignment(SwingConstants.CENTER);
		txtControl_RockerAngle.setColumns(10);
		TabControl.add(txtControl_RockerAngle);
		
		btnControl_MoveTurntable = new JButton("move turntable");
		btnControl_MoveTurntable.setBounds(20, 86, 124, 23);
		TabControl.add(btnControl_MoveTurntable);
		
		JLabel lblControl_TurntableAngle = new JLabel("Angle");
		lblControl_TurntableAngle.setFont(new Font("Calibri", Font.BOLD, 12));
		lblControl_TurntableAngle.setBounds(20, 36, 60, 20);
		TabControl.add(lblControl_TurntableAngle);
		
		JLabel lblControl_RockerAngle = new JLabel("Angle");
		lblControl_RockerAngle.setFont(new Font("Calibri", Font.BOLD, 12));
		lblControl_RockerAngle.setBounds(20, 180, 57, 20);
		TabControl.add(lblControl_RockerAngle);
		
		JLabel lblControl_RockerDirection = new JLabel("Direction");
		lblControl_RockerDirection.setFont(new Font("Calibri", Font.BOLD, 12));
		lblControl_RockerDirection.setBounds(20, 205, 57, 20);
		TabControl.add(lblControl_RockerDirection);
		
		JLabel lblControl_TurntableDirection = new JLabel("Direction");
		lblControl_TurntableDirection.setFont(new Font("Calibri", Font.BOLD, 12));
		lblControl_TurntableDirection.setBounds(20, 61, 60, 20);
		TabControl.add(lblControl_TurntableDirection);
		
		JLabel lblControl_Turntable = new JLabel("Turntable");
		lblControl_Turntable.setFont(new Font("Calibri", Font.BOLD, 12));
		lblControl_Turntable.setBounds(10, 11, 75, 20);
		TabControl.add(lblControl_Turntable);
		
		JLabel lblControl_Rocker = new JLabel("Rocker");
		lblControl_Rocker.setFont(new Font("Calibri", Font.BOLD, 12));
		lblControl_Rocker.setBounds(10, 155, 124, 20);
		TabControl.add(lblControl_Rocker);
		
		btnControl_MoveRocker = new JButton("move rocker");
		btnControl_MoveRocker.setBounds(20, 230, 124, 23);
		TabControl.add(btnControl_MoveRocker);
		
		cbControl_RockerDirection = new JComboBox<Boolean>();
		cbControl_RockerDirection.setModel(new DefaultComboBoxModel(new String[] {"true", "false"}));
		cbControl_RockerDirection.setSelectedIndex(1);
		cbControl_RockerDirection.setBounds(83, 202, 60, 22);
		TabControl.add(cbControl_RockerDirection);
		
		cbControl_TurntableDirection = new JComboBox<Boolean>();
		cbControl_TurntableDirection.setModel(new DefaultComboBoxModel(new String[] {"true", "false"}));
		cbControl_TurntableDirection.setSelectedIndex(1);
		cbControl_TurntableDirection.setBounds(83, 58, 60, 22);
		TabControl.add(cbControl_TurntableDirection);
		
		JLabel lblNewLabel = new JLabel("true = left-hand | false = right-hand");
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblNewLabel.setBounds(153, 62, 209, 14);
		TabControl.add(lblNewLabel);
		
		JLabel lblTrueDownards = new JLabel("true = downwards | false = upwards");
		lblTrueDownards.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblTrueDownards.setBounds(153, 206, 209, 14);
		TabControl.add(lblTrueDownards);

	}
}
