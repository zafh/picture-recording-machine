package agents;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import general.XML;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import services.IControlRocker;
import services.IControlTurntable;

@Agent
@Service
@ProvidedServices({
	@ProvidedService(type=IControlTurntable.class),
	@ProvidedService(type=IControlRocker.class)
})
public class HardwareBDI implements IControlTurntable, IControlRocker {
		
	//-------- parameters --------
	
	final static long Min_Pause_Turntable = 200; // micro-seconds
	final static long Max_Pause_Turntable = 200; // micro-seconds
	final static long Min_Pause_Rocker = 5000; // micro-seconds
	final static long Max_Pause_Rocker = 10000; // micro-seconds
	
	final static long OneSecond = 1000000;
	
	@Agent
	protected IInternalAccess agent;
	
	GpioController gpio;
	
	GpioPinDigitalOutput Pin_Teller_Enable;
    GpioPinDigitalOutput Pin_Teller_Dir;
    GpioPinDigitalOutput Pin_Teller_Step;
	
    GpioPinDigitalOutput Pin_Wippe_Enable;
    GpioPinDigitalOutput Pin_Wippe_Dir;
    GpioPinDigitalOutput Pin_Wippe_Step;
    
    GpioPinDigitalOutput Pin_Sensor_1_Out;
    GpioPinDigitalInput Pin_Sensor_1_In;
    GpioPinDigitalOutput Pin_Sensor_2_Out;
    GpioPinDigitalInput Pin_Sensor_2_In;
    
	protected String MqttBroker = "141.59.143.42:1883";
	protected String MqttUser = "";
	protected String MqttPw = "";
	protected MqttClient client;
	
	//-------- beliefs --------

	@Belief
	protected Boolean EmergencyStop_1;
	
	@Belief
	protected Boolean EmergencyStop_2;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {

		gpio = GpioFactory.getInstance();
		
		// https://pi4j.com/1.2/pins/model-b-plus.html
		// Pin_Teller_Enable = 22 = GPIO_6
		// Pin_Teller_Dir 	 = 23 = GPIO_14
		// Pin_Teller_Step 	 = 24 = GPIO_10
		// Pin_Wippe_Enable  = 33 = GPIO_23
		// Pin_Wippe_Dir 	 = 36 = GPIO_27
		// Pin_Wippe_Step 	 = 37 = GPIO_25
		// Pin_Sensor_1_In	 =  8 = GPIO_15
		// Pin_Sensor_1_Out	 = 12 = GPIO_01
		// Pin_Sensor_2_In   =  7 = GPIO_07
		// Pin_Sensor_2_Out  = 11 = GPIO_00
		
		// turntable
		Pin_Teller_Enable = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "Pin_Teller_Enable", PinState.LOW); //activate
		Pin_Teller_Enable.setShutdownOptions(true, PinState.HIGH); // deactivate
        Pin_Teller_Dir = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_14, "Pin_Teller_Dir", PinState.LOW);
        Pin_Teller_Dir.setShutdownOptions(true, PinState.LOW);
        Pin_Teller_Step = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_10, "Pin_Teller_Step", PinState.LOW);
        Pin_Teller_Step.setShutdownOptions(true, PinState.LOW);
		
        // rocker
        Pin_Wippe_Enable = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_23, "Pin_Wippe_Enable", PinState.LOW); //activate
        Pin_Wippe_Enable.setShutdownOptions(true, PinState.HIGH); // deactivate
        Pin_Wippe_Dir = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "Pin_Wippe_Dir", PinState.LOW); // LOW = runter ("Zahnrad (klein) nach links"), HIGH = rauf ("Zahnrad (gro�) nach rechts") 
        Pin_Wippe_Dir.setShutdownOptions(true, PinState.LOW);
        Pin_Wippe_Step = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, "Pin_Wippe_Step", PinState.LOW);
        Pin_Wippe_Step.setShutdownOptions(true, PinState.LOW);
        
        // sensor 1
        Pin_Sensor_1_Out = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, PinState.LOW);
        Pin_Sensor_1_Out.setShutdownOptions(true, PinState.LOW);
        Pin_Sensor_1_In = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_DOWN);
        Pin_Sensor_1_In.setShutdownOptions(true, PinState.LOW);
                
        // sensor 2
        Pin_Sensor_2_Out = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, PinState.LOW);
        Pin_Sensor_2_Out.setShutdownOptions(true, PinState.LOW);
        Pin_Sensor_2_In = gpio.provisionDigitalInputPin(RaspiPin.GPIO_07, PinPullResistance.PULL_DOWN);
        Pin_Sensor_2_In.setShutdownOptions(true, PinState.LOW);
        
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {
		
		// Start MQTT-Client
		try { 
			client = new MqttClient("tcp://" + MqttBroker, agent.getComponentIdentifier().toString());				 
			MqttConnectOptions options = new MqttConnectOptions();
							   options.setAutomaticReconnect(true);
							   options.setCleanSession(true);
							   options.setConnectionTimeout(10);	
							   options.setUserName("user");
							   options.setPassword("password".toCharArray());
			client.connect(options);
			client.subscribe("HardwareAgent");
			System.out.println("");
			System.out.println("Connection to MQTT-Broker (" + MqttBroker + ") established.");
		} catch (MqttException e) { 
			System.out.println("");
			System.out.println("Connection to MQTT-Broker (" + MqttBroker + ") failed!");
			System.out.println(e.getMessage());	
		}	
			
		System.out.println("Picture recording machine has been initialized.");
		System.out.println("");
		
		// Initialisierung MQTT-Listener
		client.setCallback(new MqttCallback() {
			
	        @Override
	        public void connectionLost(Throwable throwable) { }
	  
	        @Override
	        public void messageArrived(String t, MqttMessage m) throws Exception {
	        	String payload = new String (m.getPayload());	
	        	if (XML.readXML(payload, "sender").equals("PictureRecordingMachineAgent")) {
	        		MoveHardware NewGoal = new MoveHardware(payload);			
	    			agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(NewGoal);
	        	}        		        	
	        }

			@Override
			public void deliveryComplete(IMqttDeliveryToken arg0) { }
	        
	     });		
		
		/*
		
		System.out.println("Start hardware test:");
		
		System.out.println("Test Rocker (15�, downwards)");
		MoveRockerByAngle(15, true);
		BusyWait(OneSecond);
		
		System.out.println("Test Rocker (15�, upwards)");
		MoveRockerByAngle(15, false);
		BusyWait(OneSecond);
		
		System.out.println("Test Turntable (90�, left-hand)");
		MoveTurntableByAngle(90, true);
		BusyWait(OneSecond);
		
		System.out.println("Test Turntable (90�, right-hand)");
		MoveTurntableByAngle(90, false);
		BusyWait(OneSecond);
		
		System.out.println("");
		
		*/
		
	}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() throws MqttException {
		
		gpio.shutdown();
		
		client.disconnect();
		
	}
		
	//-------- goals --------
	
	@Goal(recur=true) 
	public class MoveHardware {
		
		@GoalParameter
		protected String message;
	
		public MoveHardware(String message) {
			this.message = message;
		}
		
		public String getMessage() {
			return this.message;
		}
		
		public void finishGoal() {
			this.message = "";
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return message.equals("");
		}
			
	}
	
	//-------- plans --------
	
	@Plan(trigger=@Trigger(goals=MoveHardware.class))
	protected void move(MoveHardware goal) {
				
		String message = goal.getMessage();

    	String performative = XML.readXML(message, "performative");       	
    	String content = XML.readXML(message, "content");
   	
    	double angle = Double.parseDouble(content.split(",")[0]);
    	boolean direction = Boolean.parseBoolean(content.split(",")[1]);
    	boolean result = false;

		EmergencyStop_1 = false;
		EmergencyStop_2 = false;
		
    	if (performative.equals("Request(MoveTurntable)"))
    		result = MoveTurntableByAngle(angle, direction);
    	
    	if (performative.equals("Request(MoveRocker)"))
    		result = MoveRockerByAngle (angle, direction);
    	
    	agent.getComponentFeature(IBDIAgentFeature.class).dropGoal(goal);
    	
		SendMessage(result);
		
	}	
	
	//-------- methods --------
	
	public void SendMessage(boolean result) {
		
		String topic = "PictureRecordingMachineAgent";
		
		String message = XML.createMessage(
				"Inform(Hardware)", 
				"HardwareAgent", 
				"PictureRecordingMachineAgent", 
				String.valueOf(result)
				);

		try {
			client.publish(topic, new MqttMessage(message.getBytes()));
		} catch (MqttException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public static void BusyWait(long micros) {
		
		// Quelle: "busyWaitMicros"
		// http://www.rationaljava.com/2015/10/measuring-microsecond-in-java.html, 03.09.2019
		// Datum: 28.10.2015
		
        long waitUntil = System.nanoTime() + (micros * 1_000);
        
        while(waitUntil > System.nanoTime()) { ; }
        
    }
	
	public boolean MoveTurntableByAngle (double angle, Boolean direction) {
		
		if (angle<=0) angle=0;
		if (angle>360) angle=360;
		
		System.out.println("Moving turntable (angle=" + angle + ", direction=" + direction + ")");

		// set direction
		if (direction == true)
			Pin_Teller_Dir.low();
		else // (direction == false)
			Pin_Teller_Dir.high();
		
		// calculate steps according to mechanics
		int steps = (int) (angle * (288.0 / 2.5) * (90.0 / 97.1));
		long accStep = (int) ((Max_Pause_Turntable - Min_Pause_Turntable)/(steps/2)); 
		long Pause = 0;
		
		// activate motor
		// Pin_Teller_Enable.low();

		// move turntable
		for (int i=0; i < steps; i++) {
			if (i < steps/2)
				Pause = Max_Pause_Turntable - accStep*i; // Start (geringster Geschwindigkeit = maximaler Pause)
			else
				Pause = Min_Pause_Turntable + accStep*(i-(int)(steps/2)); // H�chster Punkt (maximale Geschwindigkeit = minimale Pause)
			Pin_Teller_Step.high();
           	BusyWait(Pause);
			Pin_Teller_Step.low();
			BusyWait(Pause);
		}

		// deactivate motor
		// Pin_Teller_Enable.high();
		
		return true;
		
	}
	
	public boolean MoveRockerByAngle (double angle, Boolean direction) {
		
		if (angle<=0) angle=0;
		if (angle>90) angle=90;
		
		System.out.println("Move rocker (angle=" + angle + ", direction=" + direction + ")");
		                
		// set direction
		if (direction == true)
			Pin_Wippe_Dir.low();		
		else // (direction == false)
			Pin_Wippe_Dir.high();
		
		// calculate steps according to mechanics
		int steps = (int) (angle * (44.0/90.0) * (1600.0/18.0)); // Zahnrad (klein): 18 Z�hne, 1600 steps f�r eine Umdrehung (MotorControl: 1/8 multi-stepping) -> 1 Zahn = 1600/18, Zahnrad (gro�): 44 Z�hne auf 90� -> 1� = 44/90
		long accStep = (int) ((Max_Pause_Rocker - Min_Pause_Rocker)/(steps/2)); 
		long Pause = 0;
		
        Pin_Sensor_1_In.addListener(new GpioPinListenerDigital() {
        	@Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
        		if (direction == false && event.getState() == PinState.LOW ) {
        			EmergencyStop_1 = true;      			
        		}
        	}
        });                  
        Pin_Sensor_1_Out.setState(PinState.HIGH);
			
        Pin_Sensor_2_In.addListener(new GpioPinListenerDigital() {
        	@Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {      		
        		if (direction == true && event.getState() == PinState.LOW ) {
        			EmergencyStop_2 = true;
        		}
            }
        });      
        Pin_Sensor_2_Out.setState(PinState.HIGH);
		
		// activate motor
		// Pin_Wippe_Enable.low();
        
		// move rocker
		for (int i=0; i < steps; i++) {
			
			if ( EmergencyStop_1 == true || EmergencyStop_2 == true ) {
				System.out.println("Emergency Stop!");
				break;
			}
			
		    if (i < steps/2)
		    	Pause = Max_Pause_Rocker - accStep*i; // Start (geringster Geschwindigkeit = maximaler Pause)
		    else
		    	Pause = Min_Pause_Rocker + accStep*(i-(int)(steps/2)); // H�chster Punkt (maximale Geschwindigkeit = minimale Pause)
			Pin_Wippe_Step.high();
		    BusyWait(Pause);
			Pin_Wippe_Step.low();
			BusyWait(Pause);	
		}
        
		// deactivate motor
		// Pin_Wippe_Enable.high();	
		
		Pin_Sensor_1_In.removeAllListeners();
		Pin_Sensor_2_In.removeAllListeners();
		
		return true;
		
	}
	
	//-------- services --------

	public IFuture<Boolean> moveTurntable(double angle, Boolean direction) {
		
		final Future<Boolean> value = new Future<Boolean>();

		boolean result = MoveTurntableByAngle(angle, direction);

		value.setResult(result);		
		return value;
		
	}

	public IFuture<Boolean> moveRocker(double angle, Boolean direction) {

		final Future<Boolean> value = new Future<Boolean>();

		boolean result = MoveRockerByAngle(angle, direction);
	
		value.setResult(result);		
		return value;
		
	}
	
}