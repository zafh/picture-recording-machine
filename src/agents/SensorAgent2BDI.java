package agents;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import jadex.bdiv3.annotation.Belief;
import jadex.bridge.IInternalAccess;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;

@Agent
public class SensorAgent2BDI {
		
	//-------- parameters --------

	@Agent
	protected IInternalAccess agent;
	
	GpioController gpio;
	
	GpioPinDigitalOutput Pin_Sensor_1_Out;
	GpioPinDigitalInput Pin_Sensor_1_In;
	
	//-------- beliefs --------
    
	@Belief
	protected Boolean Looper;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
		
		gpio = GpioFactory.getInstance();
    
        // sensor
        Pin_Sensor_1_Out = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "Pin_Sensor_1_Out", PinState.LOW);
        Pin_Sensor_1_Out.setShutdownOptions(true, PinState.LOW);
        Pin_Sensor_1_In = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "Pin_Sensor_1_In", PinPullResistance.PULL_DOWN);
        Pin_Sensor_1_In.setShutdownOptions(true, PinState.LOW);
 
        Pin_Sensor_1_In.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            	Looper = false;	
            }
        });
        
        Pin_Sensor_1_Out.setState(PinState.HIGH);
                
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() throws InterruptedException {
					
		System.out.println("Sensor Agent has been initialized.");
		System.out.println("");
	
		Looper = true;
        
		while (true) {
			
			System.out.println("Running..." + "(" +  Pin_Sensor_1_Out.getState() + " | " + Pin_Sensor_1_In.getState() + ")");		
			BusyWait(1000000);
			
			if ( Looper == false) {
				System.out.println("Sensor has been triggered!");
				break;
			}
			
		}
		
	}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {
		
		gpio.shutdown();

	}
		
	//-------- goals --------
	
	//-------- plans --------			
	
	//-------- methods --------

	public static void BusyWait(long micros) {
		
		// Quelle: "busyWaitMicros"
		// http://www.rationaljava.com/2015/10/measuring-microsecond-in-java.html, 03.09.2019
		// Datum: 28.10.2015
		
        long waitUntil = System.nanoTime() + (micros * 1_000);
        
        while(waitUntil > System.nanoTime()) { ; }
        
    }
	
	//-------- services --------
	
}