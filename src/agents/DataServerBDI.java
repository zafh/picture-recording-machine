package agents;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.eclipse.paho.client.mqttv3.MqttException;
import general.DatabaseManager;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import services.IMySQL;

@Agent
@Service
@ProvidedServices({
	@ProvidedService(type=IMySQL.class)
})
public class DataServerBDI implements IMySQL {
	
	//-------- arguments --------
	
	//-------- parameters --------
	
	final static String DataPath = "";
	
	@Agent
	protected IInternalAccess agent;
	
	protected DatabaseManager DBM = new DatabaseManager();
	protected String Server = "127.0.0.1"; 
	protected String Database = "pickingstation";
	protected String User = "admin";
	protected String Password = "admin";
		
	//-------- beliefs --------

	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() throws MqttException {
		
		// establish connection to database
		DBM.connectDatabase(Server, Database, User, Password);
					
	}
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
		
	//-------- plans --------
	
	//-------- methods --------
	
	//-------- services --------
			
	@Override
	public IFuture<Boolean> executeUpdate(String update) {		
		final Future<Boolean> value = new Future<Boolean>();
		value.setResult(DBM.executeUpdate(update));		
		return value;
	}

	@Override
	public IFuture<Boolean> executeInsert(String insert) {
		final Future<Boolean> value = new Future<Boolean>();
		value.setResult(DBM.executeInsert(insert));
		return value;
	}

	@Override
	public IFuture<List<HashMap<String,Object>>> executeSelect(String select) {
		List<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
		ResultSet result = DBM.executeSelect(select);	
		try {
			while (result.next()) {
				HashMap<String,Object> row = new HashMap<String,Object>();
				Object obj;
				String col;
				for (int i=1; i<=result.getMetaData().getColumnCount(); i++) {
					obj = result.getObject(i);
					col = result.getMetaData().getColumnName(i);
					row.put(col,obj);
				}
				list.add(row);
				/*
				 * List<String> row = new ArrayList<String>();
				 * for (int i=1; i<=result.getMetaData().getColumnCount(); i++)
				 *  	row.add(result.getString(i));
				 * list.add(row);
				 */
			}
		} catch (SQLException e) {	
		}
		final Future<List<HashMap<String,Object>>> value = new Future<List<HashMap<String,Object>>>();
		value.setResult(list);
		return value;
	}
	
}