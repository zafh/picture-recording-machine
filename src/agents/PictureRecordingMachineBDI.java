package agents;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import javax.imageio.ImageIO;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import edu.ufl.digitalworlds.j4k.J4KSDK;
import general.XML;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalResult;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import main.Kinect;
import phoxi.JavaPhoXi;
import services.IMySQL;

@Agent
@RequiredServices({
	@RequiredService(name="MySQL", type=IMySQL.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_GLOBAL))
})
public class PictureRecordingMachineBDI {

	
	//-------- parameters --------

	@Agent
	protected IInternalAccess agent;
	
	IMySQL ServiceMySQL;
	
	PictureRecordingMachineGUI window;

	JavaPhoXi JavaPhoXi;
	
	protected String MqttBroker = "141.59.143.42:1883";
	protected String MqttUser = "";
	protected String MqttPw = "";
	protected MqttClient client;
	protected Kinect kinect;
	
	protected double CorrectionAngleKinect = 15.0;
	
	//-------- beliefs ---------
	
	@Belief
	protected String MessageBuffer = "";
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {

		// Start MQTT-Client
		System.out.println("");
		System.out.println("Start MQTT client...");
		try { 
			client = new MqttClient("tcp://" + MqttBroker, agent.getComponentIdentifier().toString());				 
			MqttConnectOptions options = new MqttConnectOptions();
							   options.setAutomaticReconnect(true);
							   options.setCleanSession(true);
							   options.setConnectionTimeout(10);	
							   options.setUserName("user");
							   options.setPassword("password".toCharArray());
			client.connect(options);
			client.subscribe("PictureRecordingMachineAgent");
			System.out.println("Connection to (" + MqttBroker + ") established.");
		} catch (MqttException e) { 
			System.out.println("Connection to (" + MqttBroker + ") failed!");
			System.out.println(e.getMessage());	
		}	
				
		// get service "MySQL"
		System.out.println("");
		System.out.println("Start search for provider of service MySQL...");
		IMySQL[] MySQL = agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("MySQL").get().toArray(new IMySQL[0]);					
		for(int i=0; i<MySQL.length; i++) {			
			ServiceMySQL = MySQL[i];
			System.out.println("Established connection to provider of service MySQL.");
		}

		/*
		// connect to Photoneo by PhoXi-SDK
		System.out.println("");
		JavaPhoXi = new JavaPhoXi();
		JavaPhoXi.ConnectPhoXiDevice("1712004");
		JavaPhoXi.SetModeTrigger();
		*/
		
		// connect to Kinect by J4K
		System.out.println("");
		kinect = new Kinect();
		if (kinect.start(J4KSDK.COLOR)) {
			System.out.println("Established connection to Kinect V2.");
			System.out.println("");
		} else {
			System.out.println("Connection to Kinect V2 failed!");
			System.out.println("");
		}
		
		window = new PictureRecordingMachineGUI();
		ConfigureWindow();
	
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {
		
		// Initialisierung MQTT-Listener
		client.setCallback(new MqttCallback() {
			
	        @Override
	        public void connectionLost(Throwable throwable) { }
	  
	        @Override
	        public void messageArrived(String t, MqttMessage m) throws Exception {
	        	String payload = new String (m.getPayload());	
	        	if (XML.readXML(payload, "sender").equals("HardwareAgent")) {
	        		MessageBuffer = payload;
	        	}
	        }

			@Override
			public void deliveryComplete(IMqttDeliveryToken arg0) { }
	        
	     });
		
		// run GUI
		EventQueue.invokeLater(new Runnable() {	
			public void run() {
				// open window
				window.frmPictureRecordingMachine.setVisible(true);
			}
		});
	
	}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {

		JavaPhoXi.DisconnectPhoXiDevice();
		kinect.stop();
		
	}
		
	//-------- goals --------
	
	@Goal(recur=true)
	public class WaitForResponse {
				
		@GoalTargetCondition(beliefs="MessageBuffer")
		public boolean checkTarget() {	
			String sender = "";
			if (!MessageBuffer.equals(""))
				sender = XML.readXML(MessageBuffer,"sender");
			return sender.equals("HardwareAgent");
		}
		
		@GoalResult
		public boolean finishGoal() {
			MessageBuffer = "";
			return true;
		}
		
	}
	
	//-------- plans ----------

	//-------- methods --------
	
	public IFuture<Boolean> ContactHardware(String hardware, double angle, boolean direction) {
		
		Future<Boolean> result = new Future<Boolean>();
		
		// create order to move hardware
		String topic = "HardwareAgent";
		String message = XML.createMessage(
				"Request(Move" + hardware + ")", 
				"PictureRecordingMachineAgent", 
				"HardwareAgent",
				String.valueOf(angle) + "," + String.valueOf(direction));
		
		try {
			// send message
			client.publish(topic, new MqttMessage(message.getBytes()));		
			// create goal waiting for response
			WaitForResponse NewGoal = new WaitForResponse();
			agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(NewGoal).get();
			result.setResult(true);
			return result;
		} catch (MqttException e) {
			System.out.println(e);		
			result.setResult(true);
			return result;
		}
		
	}
	
	public void StartRecording() throws IOException {
		
		if (window.tfRecording_Rocker_Intermediate.getText().equals("") || window.tfRecording_Rocker_RangeMin.getText().equals("") || window.tfRecording_Rocker_RangeMax.getText().equals("") || window.tfRecording_Turntable_Intermediate.getText().equals("")  || window.tfRecording_Turntable_RangeMin.getText().equals("") || window.tfRecording_Turntable_RangeMax.getText().equals(""))
			return;
		
		int RockerIntermediate = Integer.valueOf(window.tfRecording_Rocker_Intermediate.getText());
		double RockerRangeMin = Double.valueOf(window.tfRecording_Rocker_RangeMin.getText());
		double RockerRangeMax = Double.valueOf(window.tfRecording_Rocker_RangeMax.getText());
		int RockerSteps = RockerIntermediate + 1;
		//int RockerAngle = (int) ((RockerRangeMax-RockerRangeMin) / (RockerIntermediate+1));
		double RockerAngle = Round((RockerRangeMax-RockerRangeMin) / (RockerIntermediate+1), 2);
		String RockerPositions = RockerRangeMin + "�";
		for (int i=1; i<=RockerIntermediate; i++)
			RockerPositions = RockerPositions + " - " + Round(RockerAngle * i, 2) + "�";
		RockerPositions = RockerPositions + " - " + RockerRangeMax + "�";
		window.tfRecording_Rocker_Positions.setText(RockerPositions);
		
		int TurntableIntermediate = Integer.valueOf(window.tfRecording_Turntable_Intermediate.getText());;
		double TurntableRangeMin = Double.valueOf(window.tfRecording_Turntable_RangeMin.getText());
		double TurntableRangeMax = Double.valueOf(window.tfRecording_Turntable_RangeMax.getText());
		int TurntableSteps = TurntableIntermediate;
		//int TurntableAngle = (int) ((TurntableRangeMax-TurntableRangeMin) / (TurntableIntermediate+1));
		double TurntableAngle = Round((TurntableRangeMax-TurntableRangeMin) / (TurntableIntermediate+1), 2);
		String TurntablePositions = TurntableRangeMin + "�";
		for (int i=1; i<=TurntableIntermediate; i++)
			TurntablePositions = TurntablePositions + " - " + Round(TurntableAngle * i, 2) + "�";
		TurntablePositions = TurntablePositions + " ~ " + TurntableRangeMax + "�";
		window.tfRecording_Turntable_Positions.setText(TurntablePositions);
		
		String PictureID;
		String ArticleID = window.tfData_ID.getText();
		String Status = "0";
		String Date;
		double Rotation;
		double Angle;
		String Datapath = window.tfConfiguration_SavePathPictures.getText();
		String DatapathSQL = Datapath.replace("\\", "\\\\");
		String Filename;

		int PictureIndex = 1;
		String select = "SELECT `Index` FROM pickingstation.picture WHERE `Article`='" + ArticleID + "'";
		List<HashMap<String,Object>> result = ServiceMySQL.executeSelect(select).get();
		PictureIndex = result.size() + 1; // Very quick and dirty... But returned Index should be ok!
		
		/** Photoneo **/
		
		/* start recording
		for (int i=0; i<=RockerSteps; i++) { 
			
			if (i>0) ContactHardware("Rocker", RockerAngle, true).get();
	
			for (int j=0; j<=TurntableSteps; j++) { 
				
				// move turntable
				if (j>0) ContactHardware("Turntable", TurntableAngle, true).get();
	
				if (window.cbRecording_HardwareTest.isSelected() == false) {

					// take picture
					Filename = "pic_" + ArticleID + "_" + PictureIndex;				
					while (true) { 
						JavaPhoXi.StartTrigger(Datapath + Filename);
						File file = new File(Datapath + Filename + "_IMG_Texture_8Bit.png");	
						if (file.exists()) break;
					}

					// insert in database
					PictureID = ArticleID + "_" + PictureIndex;					
					Date = GetDateTime();
					Rotation = j * TurntableAngle;
					Angle = i * RockerAngle;
					Filename = Filename + "_IMG_Texture_8Bit";
					String insert = "INSERT INTO pickingstation.picture (`Picture-ID`,`Article`,`Index`,`Status`,`Date`,`Rotation`,`Angle`,`Datapath`,`Camera`) " 
								  + "VALUES ('" + PictureID + "','" + ArticleID + "','" + PictureIndex + "','" + Status + "','" + Date + "','" + Rotation + "','" + Angle + "','" + DatapathSQL + Filename + "','" + "PhoXi 3D Scanner M - 1712004" + "')";
					ServiceMySQL.executeInsert(insert).get();
					PictureIndex++;
					
				}
				
				if (window.cbRecording_CompleteTurning.isSelected() == true) {			
					// complete 360�-turn
					if (j == TurntableSteps) {
						// in case TurntableAngle was rounded the last step to complete 360� may be greater or less than TurntableAngle 
						double LastTurntableAngle = 360 - TurntableAngle * TurntableSteps;
						ContactHardware("Turntable", LastTurntableAngle, true).get();
					}				
				}
					
			}
			
		}
		
		// reset rocker position
		ContactHardware("Rocker", RockerAngle*RockerSteps, false).get();
		
		*/
		
		/** Kinect **/

		// modify recording angle of kinect
		ContactHardware("Rocker", CorrectionAngleKinect, true).get();
		
		// start recording
		for (int i=0; i<=RockerSteps; i++) {

			if (i>0) ContactHardware("Rocker", RockerAngle, true).get();

			for (int j=0; j<=TurntableSteps; j++) {
				
				// move turntable
				if (j>0) ContactHardware("Turntable", TurntableAngle, true).get();
				
				// take picture
				Filename = "pic_" + ArticleID + "_" + PictureIndex;
				BufferedImage image = kinect.getImage();
				File outputfile = new File(Datapath + Filename + ".png");       
				ImageIO.write(image, "PNG", outputfile);
				
				// insert in database
				PictureID = ArticleID + "_" + PictureIndex;
				Date = GetDateTime();
				Rotation = j * TurntableAngle;
				Angle = i * RockerAngle;
				String insert = "INSERT INTO pickingstation.picture (`Picture-ID`,`Article`,`Index`,`Status`,`Date`,`Rotation`,`Angle`,`Datapath`,`Camera`) " 
						  + "VALUES ('" + PictureID + "','" + ArticleID + "','" + PictureIndex + "','" + Status + "','" + Date + "','" + Rotation + "','" + Angle + "','" + DatapathSQL + Filename + "','" + "Kinect V2" + "')";
				ServiceMySQL.executeInsert(insert).get();
				PictureIndex++;

				if (window.cbRecording_CompleteTurning.isSelected() == true) {			
					// complete 360�-turn
					if (j == TurntableSteps) {
						// in case TurntableAngle was rounded the last step to complete 360� may be greater or less than TurntableAngle 
						double LastTurntableAngle = 360 - TurntableAngle * TurntableSteps;
						ContactHardware("Turntable", LastTurntableAngle, true).get();
					}				
				}
			
			}

		}
		
		// reset rocker position
		ContactHardware("Rocker", RockerAngle*RockerSteps, false).get();
		
		// reset rocker position (correction angle)
		ContactHardware("Rocker", CorrectionAngleKinect, false).get();
			
	}
	
	public void DisplayArticleData() {
	
		String ArticleName = "";
		int ID = -1;
		String Group = "";
		int Length = -1;
		int Height = -1;
		int Width = -1;
		float Mass = -1;
		long Pictures = 0;
		String Location ="";
		int ModelS = -1;
		int ModelC = -1;
		
		// get selected article
		int index = window.listData_ArticleList.getSelectedIndex();
		ArticleName = window.listModel_ArticleList.get(index);
	
		// get article data from tables "article" and "model"
		List<HashMap<String,Object>> result;
		String select = "SELECT * FROM pickingstation.article "
				      + "LEFT OUTER JOIN pickingstation.model ON article.`Article-ID`=model.`Article` "						      
				      + "WHERE `Name`='" + ArticleName + "'";
		result = ServiceMySQL.executeSelect(select).get();
		for (HashMap<String,Object> article : result) {		
			ID = (int) article.get("Article-ID");
			if (article.get("Group") != null)
				Group = (String) article.get("Group");
			if (article.get("Length") != null)
				Length = (int) article.get("Length");
			if (article.get("Height") != null)
				Height = (int) article.get("Height");
			if (article.get("Width") != null)
				Width = (int) article.get("Width");
			if (article.get("Mass") != null)
				Mass = (float) article.get("Mass");
			if (article.get("Location") != null)
				Location = (String) article.get("Location");
			if (article.get("ModelS") != null)
				ModelS = (int) article.get("ModelS");
			if (article.get("ModelC") != null)
				ModelC = (int) article.get("ModelC");
			
		}
		
		// get data from table "pictures"
		result.clear();
		select = "SELECT Count(*) FROM pickingstation.picture WHERE picture.`Article`='" + ID + "'";
		result = ServiceMySQL.executeSelect(select).get();
		Pictures = (long) result.get(0).get("Count(*)");
		
		// set article data
		window.tfData_Name.setText(ArticleName);
		window.tfData_ID.setText(Integer.toString(ID));
		window.tfData_Group.setText(Group);
		if (Length > -1) window.tfData_Length.setText(Integer.toString(Length));
		else window.tfData_Length.setText("");	
		if (Height > -1) window.tfData_Height.setText(Integer.toString(Height));
		else window.tfData_Height.setText("");
		if (Width > -1) window.tfData_Width.setText(Integer.toString(Width));
		else window.tfData_Width.setText("");
		if (Mass > -1) window.tfData_Mass.setText(Float.toString(Mass));
		else window.tfData_Mass.setText("");
		window.tfData_Pictures.setText(Long.toString(Pictures));
		window.tfData_Location.setText(Location);
		if (ModelS > -1) ; 		// TBD
		else ; 					// TBD
		if (ModelC > -1) ; 		// TBD
		else ; 					// TBD
		window.lblRecording_Article.setText(ArticleName + " (ID=" + Integer.toString(ID) + ")");
		
	}
	
	public void ConfigureWindow() {
		
		// change displayed turntable positions for recording
		window.tfRecording_Turntable_Intermediate.getDocument().addDocumentListener(new DocumentListener() {
		    public void insertUpdate(DocumentEvent e) { update(); }
		    public void removeUpdate(DocumentEvent e) { update(); }
		    public void changedUpdate(DocumentEvent e) { update(); }
		    private void update() {
				if (window.tfRecording_Turntable_Intermediate.getText().equals("")  || window.tfRecording_Turntable_RangeMin.getText().equals("") || window.tfRecording_Turntable_RangeMax.getText().equals("")) {
					window.tfRecording_Turntable_Positions.setText("");
					return;
				}
				int TurntableIntermediate = Integer.valueOf(window.tfRecording_Turntable_Intermediate.getText());;
				double TurntableRangeMin = Double.valueOf(window.tfRecording_Turntable_RangeMin.getText());
				double TurntableRangeMax = Double.valueOf(window.tfRecording_Turntable_RangeMax.getText());
				//int TurntableAngle = (int) ((TurntableRangeMax-TurntableRangeMin) / (TurntableIntermediate+1));
				double TurntableAngle = Round((TurntableRangeMax-TurntableRangeMin) / (TurntableIntermediate+1), 2);
				String TurntablePositions = TurntableRangeMin + "�";
				for (int i=1; i<=TurntableIntermediate; i++)
					TurntablePositions = TurntablePositions + " - " + Round(TurntableAngle * i, 2) + "�";
				TurntablePositions = TurntablePositions + " ~ " + TurntableRangeMax + "�";
				window.tfRecording_Turntable_Positions.setText(TurntablePositions);		
		    }
		});
		
		// change displayed rocker positions for recording
		window.tfRecording_Rocker_Intermediate.getDocument().addDocumentListener(new DocumentListener() {
		    public void insertUpdate(DocumentEvent e) { update(); }
		    public void removeUpdate(DocumentEvent e) { update(); }
		    public void changedUpdate(DocumentEvent e) { update(); }
		    private void update() {
				if (window.tfRecording_Rocker_Intermediate.getText().equals("") || window.tfRecording_Rocker_RangeMin.getText().equals("") || window.tfRecording_Rocker_RangeMax.getText().equals("")) {
					window.tfRecording_Rocker_Positions.setText("");
					return;
		   		}
				int RockerIntermediate = Integer.valueOf(window.tfRecording_Rocker_Intermediate.getText());
				double RockerRangeMin = Double.valueOf(window.tfRecording_Rocker_RangeMin.getText());
				double RockerRangeMax = Double.valueOf(window.tfRecording_Rocker_RangeMax.getText());
				//int RockerAngle = (int) ((RockerRangeMax-RockerRangeMin) / (RockerIntermediate+1));
				double RockerAngle = Round((RockerRangeMax-RockerRangeMin) / (RockerIntermediate+1), 2);
				String RockerPositions = RockerRangeMin + "�";
				for (int i=1; i<=RockerIntermediate; i++)
					RockerPositions = RockerPositions + " - " + Round(RockerAngle * i, 2) + "�";
				RockerPositions = RockerPositions + " - " + RockerRangeMax + "�";
				window.tfRecording_Rocker_Positions.setText(RockerPositions);		
		    }
		});

		// change displayed rocker positions for recording
		window.tfRecording_Rocker_RangeMax.getDocument().addDocumentListener(new DocumentListener() {
		    public void insertUpdate(DocumentEvent e) { update(); }
		    public void removeUpdate(DocumentEvent e) { update(); }
		    public void changedUpdate(DocumentEvent e) { update(); }
		    private void update() {
				if (window.tfRecording_Rocker_Intermediate.getText().equals("") || window.tfRecording_Rocker_RangeMin.getText().equals("") || window.tfRecording_Rocker_RangeMax.getText().equals("")) {
					window.tfRecording_Rocker_Positions.setText("");
					return;
		   		}
				int RockerIntermediate = Integer.valueOf(window.tfRecording_Rocker_Intermediate.getText());
				double RockerRangeMin = Double.valueOf(window.tfRecording_Rocker_RangeMin.getText());
				double RockerRangeMax = Double.valueOf(window.tfRecording_Rocker_RangeMax.getText());
				//int RockerAngle = (int) ((RockerRangeMax-RockerRangeMin) / (RockerIntermediate+1));
				double RockerAngle = Round((RockerRangeMax-RockerRangeMin) / (RockerIntermediate+1), 2);
				String RockerPositions = RockerRangeMin + "�";
				for (int i=1; i<=RockerIntermediate; i++)
					RockerPositions = RockerPositions + " - " + Round(RockerAngle * i, 2) + "�";
				RockerPositions = RockerPositions + " - " + RockerRangeMax + "�";
				window.tfRecording_Rocker_Positions.setText(RockerPositions);		
		    }
		});

		// initialize list of articles
		String select = "SELECT * FROM pickingstation.article WHERE `Article-ID` >= 20";
		List<HashMap<String,Object>> ArticleList = ServiceMySQL.executeSelect(select).get();				
		for (HashMap<String,Object> article : ArticleList)
			window.listModel_ArticleList.addElement((String) article.get("Name"));

		// change displayed information of selected article
		window.listData_ArticleList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent eve) {
				DisplayArticleData();
			}
		});
			
		// administration of articels: create new article
		window.btnData_New.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
			}
		});
		
		// administration of articles: change properties of selected article
		window.btnData_Edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
			}
		});
		
		// administration of articles: apply chances to properties of selected article
		window.btnData_Apply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
			}
		});
		
		// control of picture recording: start recording
		window.btnRecording_Start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try { StartRecording(); } 
				catch (IOException e1) { e1.printStackTrace(); }
			}
		});
		
		// configuration: change picture path
		window.btnConfiguration_SavePathPictures.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
			}
		});
		
		// control picture recording machine: move turntable
		window.btnControl_MoveTurntable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			boolean direction = false;
			double angle = Double.valueOf(window.txtControl_TurntableAngle.getText());			
			String item = (String) window.cbControl_TurntableDirection.getSelectedItem();
			if (item.equals("true")) direction = true;
			if (item.equals("false")) direction = false;
			if (angle>0)
				ContactHardware("Turntable", angle, direction).get();
			}
		});
		
		// control picture recording machine: move rocker
		window.btnControl_MoveRocker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean direction = false;
				double angle = Double.valueOf(window.txtControl_RockerAngle.getText());
				String item = (String) window.cbControl_RockerDirection.getSelectedItem();
				if (item.equals("true")) direction = true;
				if (item.equals("false")) direction = false;
				if (angle>0)
					ContactHardware("Rocker", angle, direction).get();
			}
		});
		
	}
	
	public String GetDateTime() {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
		return formatter.format(new Date());	
	}
	
	public String GetTime() {
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
		return formatter.format(new Date());	
	}
	
	public void SetStatus(String text) {
		String status;
		status = GetTime() + " | " + text;
		window.taStatus.append(status);
	}
	
	public double Round(double value, double number) {
		
		return Math.floor(value * Math.pow(10, number)) / Math.pow(10, number);
		
	}
	
	//-------- services --------
	
}