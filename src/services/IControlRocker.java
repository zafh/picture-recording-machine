package services;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IControlRocker {
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> moveRocker(double angle, Boolean direction);
	
}
