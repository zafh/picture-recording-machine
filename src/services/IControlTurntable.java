package services;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IControlTurntable {

	@Timeout(Timeout.NONE)
	public IFuture<Boolean> moveTurntable(double angle, Boolean direction);
	
}
