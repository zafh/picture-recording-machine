package general;

import java.util.HashMap;

public class XML {
	
	public static String readXML (String xml, String attribute) {
		
		switch (attribute) {		
			case "performative":	return xml.substring(xml.indexOf("<performative>") + "<performative>".length(), xml.indexOf("</performative>"));						
			case "sender":			return xml.substring(xml.indexOf("<sender>") + "<sender>".length(), xml.indexOf("</sender>"));
			case "receiver":		return xml.substring(xml.indexOf("<receiver>") + "<receiver>".length(), xml.indexOf("</receiver>"));
			case "content":			return xml.substring(xml.indexOf("<content>") + "<content>".length(), xml.indexOf("</content>"));
		}
		
		return "";
		
	}
	
	public static HashMap<String,String> readXML (String xml) {
		
		HashMap<String,String> message = new HashMap<String,String>();

		String performative = xml.substring(xml.indexOf("<performative>") + "<performative>".length(), xml.indexOf("</performative>"));	
		message.put("performative", performative);
		
		String sender = xml.substring(xml.indexOf("<sender>") + "<sender>".length(), xml.indexOf("</sender>"));
		message.put("sender", sender);
		
		String receiver = xml.substring(xml.indexOf("<receiver>") + "<receiver>".length(), xml.indexOf("</receiver>"));	
		message.put("receiver", receiver);
		
		String content= xml.substring(xml.indexOf("<content>") + "<content>".length(), xml.indexOf("</content>"));	
		message.put("content", content);

		return message;

	}
	
	public static String createMessage(String performative, String sender, String reveiver, String content) {
		
		String message = "";
		
		message = "<message>" +
					"<performative>" + performative + "</performative>" +
					"<sender>" + sender + "</sender>" +
					"<receiver>" + reveiver + "</receiver>" +
					"<content>" + content + "</content>" +
				  "</message>";

		return message;
		
	}
	
}
