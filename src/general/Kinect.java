package general;

import java.awt.Color;
import java.awt.image.BufferedImage;
import edu.ufl.digitalworlds.j4k.J4KSDK;

public class Kinect extends J4KSDK{
 
    BufferedImage image = null;
    
    public BufferedImage getImage() { return this.image; }
    
    @Override
    public void onDepthFrameEvent(short[] depth_frame, byte[] player_index, float[] XYZ, float[] UV) {}

    @Override
    public void onSkeletonFrameEvent(boolean[] skeleton_tracked, float[] joint_position, float[] joint_orientation, byte[] joint_status) {}

    @Override
    public void onColorFrameEvent(byte[] data) {    

    	int width = getColorWidth();
    	int height = getColorHeight();

    	image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        
    	int idx=0;
    	
    	for(int y=0; y < height; y++)	
    	for(int x=0; x < width; x++)		
		{
			image.setRGB(width-1-x,y,(new Color(data[idx+2]&0xFF, data[idx+1]&0xFF, data[idx+0]&0xFF)).getRGB());
			idx+=4;
		}
    	
    }
       
} 
