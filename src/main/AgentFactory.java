package main;

import java.util.HashMap;
import jadex.base.PlatformConfiguration;
import jadex.base.Starter;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;

public class AgentFactory {
	
    public static void main(String[] args) {
    	
    	String AgentName = "";
    	String AgentClass = "";
        
    	int AgentValue = 3;
    	
    	switch (AgentValue) {
    	
    		case 1:	AgentName =  "DataServer";
    				AgentClass = "agents.DataServerBDI.class";
    				break;
    		
    		case 2:	AgentName =  "Hardware";
					AgentClass = "agents.HardwareBDI.class";
    				break;
    				
    		case 3:	AgentName =  "PictureRecordingMachine";
					AgentClass = "agents.PictureRecordingMachineBDI.class";
    				break;
    				
    		case 4: AgentName = "YoloData";
    				AgentClass = "agents.YoloDataBDI.class";
    				break;
    				
    		case 5: AgentName = "SensorAgent";
    				AgentClass = "agents.SensorAgent2BDI.class";
    				break;
    				
    		case 6: AgentName = "MqttAgent";
    				AgentClass = "agents.MqttAgentBDI.class";
    				break;
    				
    	}

    	// configure agent plattform
    	PlatformConfiguration config = PlatformConfiguration.getDefaultNoGui();
	    	config.setPlatformName("PRM-System_" + AgentName);
	    	config.getRootConfig().setNetworkName("MyPRMNetworkName");
	    	config.getRootConfig().setNetworkPass("MyPRMNetworkPassword");
	    	config.getRootConfig().setTrustedLan(true);
	    	config.getRootConfig().setAwareness(true);
	    	config.getRootConfig().setAwaMechanisms(PlatformConfiguration.AWAMECHANISM.local, PlatformConfiguration.AWAMECHANISM.broadcast, PlatformConfiguration.AWAMECHANISM.multicast);
	    
	    // start agent plattform
    	IExternalAccess platform = Starter.createPlatform(config).get();   	
        IComponentManagementService cms = SServiceProvider.getService(platform, IComponentManagementService.class).get();     
        
        HashMap<String, Object> AgentData = new HashMap<String, Object>();
        //AgentData.put("MyObject", null);
        
	    AgentData.clear();
		cms.createComponent(AgentName, AgentClass, new CreationInfo(AgentData)).getFirstResult();
				
    }
    
}