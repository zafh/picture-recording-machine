# Picture Recording Machine #

### Beschreibung ###
Die Bildaufnahmestation (Picture Recording Machine = PRM) wurde entwickelt, um eine kontrollierte und größtenteils automatisierte Aufnahme von Bildern von Objekten durch eine Microsoft Kinect (V2) und einen Scanner von Photoneo für das Training von Neuronalen Netzen zur Objekterkennung zu schaffen.

### Komponenten (Hardware) ###
Die Hardware der PRM wird durch einen RaspberryPi via GPIO kontrolliert. Dieser ist via MQTT-Broker, der ebenfalls auf einem RaspberryPi installiert ist, mit einem Desktop-PC verbunden, der neben einem MySQL-Server für die Datenverwaltung eine Steuerungssoftware enthält, um den Aufnahmeprozess zu parametrieren und zu steuern.

### Systemanforderungen ###
- Das Tutorial verwendet Jadex Version 3.0.115. Eine damit kompatible Java-Version ist 1.8.0_261.
- Für den Zugriff auf die Kinect wird J4KSDK verwendet.
- Die Hardware der Aufnahmestation wird via GPIO angesteuert und durch pi4j (1.2) in Java integriert.
- Die Photoneo-Kamera wird durch die Software PhoXiControl kontrolliert. Dieses stellt eine API in C++ bereit, die via JNI in Java integriert wird.

### Steuerung (Software) ###
Bis auf den MQTT-Broker, der via NodeRed mit einem Mosca-Broker eingerichtet ist, wird jede Komponente durch einen BDI-Agenten (JADEX/JADEX) gesteuert, der die Hardware kontrolliert und mit den anderen Agenten kommuniziert. Das Multiagenten-System sowie die Benutzeroberfläche sind in der Dokumentation (Downloads -> Dokumentation_Bildaufnahme.pdf) beschrieben.

### Multiagneten-System ###
- Hardware-Agent (Aufnahmestation) 
- DateServer-Agent (MySQL-Server)
- PRM-Agent (Benutzeroberfläche, Aufnahmeverwaltung)

### Acknowledgment ###
This work is part of the project “ZAFH Intralogistik”, funded by the European Regional Development Fund and the Ministry of Science, Research and Arts of Baden-Württemberg, Germany (F.No. 32-7545.24-17/3/1). It is also done within the post graduate school “Cognitive Computing in Socio-Technical Systems“ of Ulm University of Applied Sciences and Ulm University, which is funded by Ministry for Science, Research and Arts of the State of Baden-Württemberg, Germany.
